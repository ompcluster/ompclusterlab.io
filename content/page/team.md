---
title: Team
subtitle: Academic Researchers
comments: false
---

Here is a tentative of an exhaustive list of people involved in the project.

### Faculty members

   - Prof. Dr. [Guido Araújo](https://guidoaraujo.wordpress.com/) (Project Lead)
   - Prof. Dr. [Hervé Yviquel](https://ic.unicamp.br/~herve/)
   - Dr. [Marcio Machado Pereira](https://iviarcio.wordpress.com)
   - Prof. Dr. [Emilio Francisquini](http://professor.ufabc.edu.br/~e.francesquini/) (UFABC)
   - Prof. Dr. [Sandro Rigo](https://www.ic.unicamp.br/~sandro/)

### Graduated students

   - Gustavo Leite
   - Carla Cardoso Cusihuallpa
   - Guilherme Valarini
   - Rodrigo Ceccato de Freitas
   - Pedro Henrique Di Francia Rosso
   - Vitoria Dias Moreira Pinho
   - Rémy Neveu

### Undergraduate students

   - Jhonatan Cléto
   - Bruno Tojo da Silva

### Former members

   - Prof. Dr. [Eduardo Candido Xavier](https://www.ic.unicamp.br/~eduardo/)
   - Lucas de Oliveira Silva
   - Willian Takayuki Ozako
   - Leonardo Picoli
   - Guilherme Perrotta
   - Sophia Estrela
   - Giovanna Vendramini
   - Victor Andrietta Razoli Silva

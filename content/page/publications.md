---
layout: page
title: Publications
permalink: /publications/
---

If you use or somehow benefit from OmpCluser (OMPC) in your research, please cite the following paper in your publication (and let us know about your work, we are happy to add your paper to this page):

  - H. Yviquel, M. Pereira, E. Francesquini, G. Valarini, G. Leite, P. Rosso, R. Ceccato, C. Cusihualpa, V. Dias, S. Rigo, A. Sousa, and G. Araujo. "The OpenMP Cluster
  Programming Model". In Proceedings of 51st International Conference on Parallel Processing Workshop (ICPP Workshops’22). ACM, New York, NY. [[pdf](https://arxiv.org/abs/2207.05677)]

```latex
@article{OMPC,
    title = {{The OpenMP Cluster Programming Model}},
    author = {Hervé Yviquel and Marcio Pereira and Emílio Francesquini and Guilherme Valarini and Gustavo Leite, Pedro Rosso and Rodrigo Ceccato and Carla Cusihualpa and Vitoria Dias and Sandro Rigo and Alan Souza and Guido Araujo},
    year = {2022},
    journal = {51st International Conference on Parallel Processing Workshop Proceedings (ICPP Workshops 22)}
```

---

Publications about or using OmpCluster are listed here:

  - C. Cardoso, H. Yviquel, G. Valarini, G. Leite, R. Ceccato, M. Pereira, A. Souza, and G. Araujo. "An OpenMP-only Linear Algebra Library for Distributed Architectures". 2022 IEEE 34nd International Symposium on Computer Architecture and High Performance Computing Workshop (SBAC-PADW). [to appear]
  - R. Ceccato, H. Yviquel, M. Pereira, A. Souza, and G. Araujo. "Implementing the Broadcast Operation in a Distributed Task-based Runtime". 2022 IEEE 34nd International Symposium on Computer Architecture and High Performance Computing Workshop (SBAC-PADW). [to appear]
  - C. Cardoso, H. Yviquel, G. Valarini, G. Leite, M. Pereira, A. Souza, and G. Araujo. "An OpenMP-only Linear Algebra Library for Distributed Architectures". Latin American High Performance Computing Conference (CARLA 2022). Short paper [to appear].
  - V. Pinho, H. Yviquel, G. Valarini, G. Leite, M. Pereira, A. Souza, and G. Araujo. "Profiling Analysis for a Distributed Task-Based Runtime". Latin American High Performance Computing Conference (CARLA 2022). Short paper [to appear].
  - H. Yviquel, M. Pereira, E. Francesquini, G. Valarini, G. Leite, P. Rosso, R. Ceccato, C. Cusihualpa, V. Dias, S. Rigo, A. Sousa, and G. Araujo. "The OpenMP Cluster
  Programming Model". In Proceedings of 51st International Conference on Parallel Processing Workshop (ICPP Workshops’22). ACM, New York, NY. [[pdf](https://arxiv.org/abs/2207.05677)]
  - P. Rosso, and E. Francesquini. "OCFTL: An MPI Implementation-Independent Fault Tolerance Library for Task-Based Applications". In: Latin American High Performance Computing Conference. Springer, Cham, 2022. p. 131-147. [[link](https://link.springer.com/chapter/10.1007/978-3-031-04209-6_10)]
  - P. Rosso, and E. Francesquini. "Improved Failure Detection and Propagation Mechanisms for MPI." Anais da XII Escola Regional de Alto Desempenho de São Paulo. SBC, 2021. [[link](https://doi.org/10.5753/eradsp.2021.16702)]
  - P. Rosso, and E. Francesquini. "A Fault Tolerant Scheduling Model for Directed Acyclic Graphs in Cloud." Anais da XI Escola Regional de Alto Desempenho de São Paulo. SBC, 2020. [[link](https://doi.org/10.5753/eradsp.2020.16883)]
  - V. Pinho, H. Yviquel, M. Machado Pereira and G. Araujo, "OmpTracing: Easy Profiling of OpenMP Programs", 2020 IEEE 32nd International Symposium on Computer Architecture and High Performance Computing (SBAC-PAD), 2020, pp. 249-256. [[link](https://doi.org/10.1109/SBAC-PAD49847.2020.00042)]

---

Dissertation and thesis:

  - Rodrigo Ceccato de Freitas. "Implementation and optimization of broadcast communication in the OmpCluster task-based runtime" (2022). MSc dissertation. [[link](https://hdl.handle.net/20.500.12733/5622)]
  - Pedro Henrique Di Francia Rosso. "OCFTL: an MPI implementation-independent fault tolerance library for task-based applications" (2021). MSc dissertation. [[link](https://pedroohr.github.io/documents/thesis.pdf)]

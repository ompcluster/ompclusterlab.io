---
title: About the project
subtitle: Brazilian Research in Parallel Computing
comments: false
---

OmpCluster is a research project conducted at the [LSC lab](http://lsc.ic.unicamp.br/)
from the University of Campinas ([IC-UNICAMP](http://ic.unicamp.br/)) in Brazil.

### Founding

The project is financially supported by:

 - Petróleo Brasileiro S.A - Petrobras under grant 2018/00347-4,
 - the Center for Computing in Engineering and Sciences (CCES) and São Paulo
 Research Foundation (FAPESP) under grant 2020/08475-1,
 - the National Council for Scientific and Technological Development (CNPq)
 under grant 402467/2021-3.

### Contact us

[ompcluster@gmail.com](mailto:ompcluster@gmail.com)

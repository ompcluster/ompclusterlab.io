---
title: OMPC PLASMA
subtitle: Distributed Task-based Linear Algebra Library
comments: false
---

OMPC PLASMA is an extension of the PLASMA library for distributed memory systems. Similar to PLASMA, OMPC PLASMA represents tiled algorithms through task graphs. Where the nodes of the graph correspond to BLAS kernels and edges to data dependencies between the kernel computations.

OMPC PLASMA has a set of parallel linear algebra routines that execute in parallel on distributed systems. It consists of OMPC runtime and PLASMA components. The OMPC runtime is responsible for managing resources and distributing work. The PLASMA component contains the routines for processing matrices. It uses the BLAS and LAPACK libraries to perform low-level procedures like basic linear algebra operations, such as matrix-matrix multiplication, matrix addition, or row swapping. This component uses OpenMP to parallelize these procedures and leverage performance from multi-core architectures. 

To execute PLASMA on distributed systems, we modified the PLASMA kernels to offload the computation across the remote worker nodes. As the execution is offloaded,  OMPC  PLASMA also leverages the multi-core resources from each node. 

An example of how to use OMPC PLASMA is included in the [OmpCluster ReadTheDocs](https://ompcluster.readthedocs.io).

You can download the OMPC PLASMA library [here](https://gitlab.com/ompcluster/plasma).

---
title: New major release v1.0.0
date: 2020-08-19
---

We just published the v1.0.0 release of our compiler/runtime
infrastructure, you can find the details
[here](https://gitlab.com/ompcluster/llvm-project/-/releases/v1.0.0).

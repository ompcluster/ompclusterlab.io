---
title: OmpTracing paper accepted to WAMCA20
date: 2020-08-20
---

Great news! The paper presenting OmpTracing has been accepted to [WAMCA 2020](http://www.cri.ensmp.fr/conf/wamca2020/).
Please register to the SBAC-PAD conference and participate to the workshop if
you want to know more about it.

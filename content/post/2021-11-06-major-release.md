---
title: New major release v12.7.0
date: 2021-11-06
---

We just published the v12.7.0 release of our compiler/runtime infrastructure
which is now based on LLVM 12, you can find the details
[here](https://gitlab.com/ompcluster/llvm-project/-/releases/v12.7.0). The
performance of the runtime has been globally improved.

As you might have noticed, we decided to change the way it is versionned. It is
now following this pattern: `<LLVM version>.<OMPC version>.<Patch version>`.
This will make it easier to separate the evolution of LLVM and OMPC.

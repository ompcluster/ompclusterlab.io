---
title: Upstream contributions
date: 2022-09-11
---

Let me announce that our student Guilherme Valarini started to send patches of
our work to the upstream LLVM repository. Check the LLVM phabricator to know
more about it ([D132676](https://reviews.llvm.org/D132676) and [D132005](https://reviews.llvm.org/D132005)).

Be ready, more patches should come soon.

---
title: Regular releases
date: 2021-04-23
---

Just a note to mention the project is not dead, we are making new releases
regularly that you can follow [here](https://gitlab.com/ompcluster/llvm-project/-/releases)
(the current one is v1.3.0). The runtime is still being heavily developed, we
will introduce it in an upcoming research conference as soon as we are confident
enough with the results.

---
title: First Official Release!
date: 2019-10-11
---

We just released the first official version of our compiler/runtime
infrastructure, you can find the details
[here](https://gitlab.com/ompcluster/llvm-project/-/releases/v0.2.0).

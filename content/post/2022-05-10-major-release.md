---
title: New major release v14.9.0
date: 2022-05-10
---

We just published the v14.9.0 release of our compiler/runtime
infrastructure which is now based on LLVM 14, you can find the details
[here](https://gitlab.com/ompcluster/llvm-project/-/releases/v14.9.0). The
performance of the runtime has been globally improved.

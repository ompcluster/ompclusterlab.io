---
title: LLPP publication
date: 2022-07-11
---

Good news, our paper about the OMPC runtime was accepted to the Second Workshop
on LLVM in Parallel Processing ([LLPP](https://llvm.org/devmtg/2022-08-29/)),
that will be held at [ICPP 2022](https://icpp22.gitlabpages.inria.fr).

---
title: New publications
date: 2022-09-22
---

More good news, I am happy to announced that we got multiple papers accepted
recently, two posters / short-papers submitted to [CARLA22](http://carla2022.org/)
which will happen in Porto Alegre (Brazil) and two papers submitted to
[WAMCA](https://www.cri.ensmp.fr/conf/wamca2022/) in Bordeaux (France).
Check the publication page to get the exact references.

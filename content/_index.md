## Cluster Programming Made Easy!

Our toolset allows the programming of heterogeneous clusters using OpenMP only.
No need to learn MPI anymore: only describe data movements and parallelism using
OpenMP directives, and our runtime will handle data distribution and load
balancing automatically!

---
title: OmpTracing
subtitle: Easy profiling of OpenMP programs!
comments: false
---

OmpTracing is a profiling tools for OpenMP programs.

OmpTracing user manual is included in the [OmpCluster ReadTheDocs](https://ompcluster.readthedocs.io).

You can download OmpTracing [here](https://gitlab.com/ompcluster/omptracing).
